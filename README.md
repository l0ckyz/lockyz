### Hi there 👋

- 🔭 I’m currently working on a few discord bots under the names of Dismon and JoiBoi.
- 🌱 I’m currently learning JavaScript
- 📫 How to reach me: [email](mailto:dev@lockyzdev.net) [Discord](https://discord.gg/eRPsZns) [Twitter](https://twitter.com/l0ckyz) [Website](https://lockyzdev.net/)
- 😄 Pronouns: They/Them
- ⚡ Fun fact: I did gymnastics as a child.
